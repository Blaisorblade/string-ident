# Ltac2 implementation of `string_to_ident`

This "Iris plugin" implements the `string_to_ident` hook of the Iris proof mode
using Ltac2, providing support for Gallina names in Iris intro patterns on Coq
8.11 and later.

## Installation

You can install this "plugin" via opam. First add the Iris opam repository:

```sh
opam repo add iris-dev https://gitlab.mpi-sws.org/iris/opam.git
```

Then install with `opam install coq-iris-string-ident`.

## Usage

Simply require this file before using the feature in the proof mode:

```coq
From iris_string_ident Require ltac2_string_ident.
From iris.proofmode Require Import tactics intro_patterns.

Lemma sep_demo {PROP: sbi} (P1: PROP)  (P2 P3: Prop) (Himpl: P2 -> P3) :
  P1 ∗ ⌜P2⌝ -∗ P1 ∗ ⌜P3⌝.
Proof.
  iIntros "[HP %HP2]".
  iFrame.
  iPureIntro.
  exact (Himpl HP2).
Qed.
```

## Acknowledgements

This implementation strategy was suggested by Robbert Krebbers.
